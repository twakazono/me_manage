package me.manage.backend.service

import me.manage.backend.dao.StandardDao
import me.manage.backend.model.Nutrition
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Service

@Service
class StandardService(@Qualifier("fake") private val standardDao: StandardDao) {
    fun getStandard(): Nutrition {
        return standardDao.getStandard()
    }
}
