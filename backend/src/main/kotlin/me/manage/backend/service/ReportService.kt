package me.manage.backend.service

import me.manage.backend.dao.ReportDao
import me.manage.backend.model.Nutrition
import me.manage.backend.model.Report
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Service
import java.util.*

@Service
class ReportService(@Qualifier("report_postgres")
                    private val reportDao: ReportDao) {
    fun addReport(report: Report) {
        return reportDao.addReport(report)
    }

    fun getAllReports(): List<Report> {
        val reports = reportDao.selectAllReports()
        return reports.sortedBy { it.getDate() }
    }

    fun getLastWeekReports(): List<Report> {
        return reportDao.selectLastWeekReports()
    }

    fun getReportsByDate(date: Date): List<Report> {
        return reportDao.selectReportsByDate(date)
    }
}
