package me.manage.backend.dao

import me.manage.backend.model.Food
import me.manage.backend.model.Nutrition
import org.springframework.jdbc.core.RowMapper
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Repository

@Repository("food_postgres")
class FoodDaoPostgres(private val jdbcTemplate: NamedParameterJdbcTemplate) :
        FoodDao {
    private val rowMapper = RowMapper { rs, _ ->
        Food(rs.getInt("id"),
                rs.getString("name"),
                rs.getInt("cost"),
                Nutrition(rs.getFloat("energy"),
                        rs.getFloat("protein"),
                        rs.getFloat("lipid"),
                        rs.getFloat("carbohydrate"),
                        rs.getFloat("sodium")))
    }

    override fun addFood(food: Food) {
        val sql = "INSERT INTO food (name, cost, energy, protein, lipid, " +
                "carbohydrate, sodium) VALUES (:n, :x :e, :p, :l, :c, :s)"
        val nutrition = food.nutrition
        val params = MapSqlParameterSource()
                .addValue("n", food.name)
                .addValue("x", food.cost)
                .addValue("e", nutrition.energy)
                .addValue("p", nutrition.protein)
                .addValue("l", nutrition.lipid)
                .addValue("c", nutrition.carbohydrate)
                .addValue("s", nutrition.sodium)
        jdbcTemplate.update(sql, params)
    }

    override fun findAllFood(): List<Food> {
        val sql = "SELECT * FROM food"
        return jdbcTemplate.query(sql, rowMapper)
    }

    override fun findFoodByName(name: String): List<Food> {
        val sql = "SELECT * FROM food WHERE name = :n"
        return jdbcTemplate.query(sql,
                MapSqlParameterSource().addValue("n", name),
                rowMapper)
    }
}
