package me.manage.backend.dao

import me.manage.backend.model.Nutrition
import me.manage.backend.model.Report
import java.util.*

interface ReportDao {
    fun addReport(report: Report)
    fun selectAllReports(): List<Report>
    fun selectLastWeekReports(): List<Report>
    fun selectReportsByDate(ate_at: Date): List<Report>
}
