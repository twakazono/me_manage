package me.manage.backend.dao

import me.manage.backend.model.Food

interface FoodDao {
    fun addFood(food: Food)
    fun findAllFood(): List<Food>
    fun findFoodByName(name: String): List<Food>
}
