package me.manage.backend.api

import me.manage.backend.model.Nutrition
import me.manage.backend.service.RecommendService
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("api/v1/recommend")
@CrossOrigin(origins = ["http://localhost:8080"])
class RecommendController(private val recommendService: RecommendService) {
    @GetMapping()
    fun getRecOrderFoodList(@RequestParam energy: Float,
                            @RequestParam protein: Float,
                            @RequestParam lipid: Float,
                            @RequestParam carbohydrate: Float,
                            @RequestParam sodium: Float) =
            recommendService.getRecOrderFoodList(
                    Nutrition(energy, protein, lipid, carbohydrate, sodium))

    @GetMapping("new")
    fun getRecWithinBudget(@RequestParam budget: Int,
                           @RequestParam energy: Float,
                           @RequestParam protein: Float,
                           @RequestParam lipid: Float,
                           @RequestParam carbohydrate: Float,
                           @RequestParam sodium: Float) =
            recommendService.getRecWithinBudget(
                    budget,
                    Nutrition(energy, protein, lipid, carbohydrate, sodium))
}
