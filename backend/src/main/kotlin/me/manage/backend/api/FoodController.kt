package me.manage.backend.api

import me.manage.backend.model.Food
import me.manage.backend.service.FoodService
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("api/v1/food")
@CrossOrigin(origins = ["http://localhost:8080"])
class FoodController(private val foodService: FoodService) {
    @PostMapping
    fun addFood(@RequestBody food: Food) = foodService.addFood(food)

    @GetMapping
    fun getFoodList() = foodService.getFoodList()

    @GetMapping(path = ["{name}"])
    fun findFoodByName(@PathVariable("name") name: String) =
            foodService.findFoodByName(name)

}
