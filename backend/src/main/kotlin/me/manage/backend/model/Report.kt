package me.manage.backend.model

import java.util.*

class Report(private val nutrition: Nutrition, private val name: String,
             private val ate_at: Date) {
    fun getNutrition() = nutrition
    fun getName() = name
    fun getDate() = ate_at
}

