package me.manage.backend.model

import kotlin.math.abs

class Nutrition(
        var energy: Float,
        var protein: Float,
        var lipid: Float,
        var carbohydrate: Float,
        var sodium: Float) {
    operator fun plus(r: Nutrition) =
            Nutrition(energy + r.energy, protein + r.protein, lipid + r.lipid,
                    carbohydrate + r.carbohydrate, sodium + r.sodium)

    operator fun minus(r: Nutrition) =
            Nutrition(energy - r.energy, protein - r.protein, lipid - r.lipid,
                    carbohydrate - r.carbohydrate, sodium - r.sodium)

    operator fun div(r: Nutrition) =
            Nutrition(energy / r.energy, protein / r.protein, lipid / r.lipid,
                    carbohydrate / r.carbohydrate, sodium / r.sodium)

    operator fun div(r: Int) =
            Nutrition(energy / r, protein / r, lipid / r,
                    carbohydrate / r, sodium / r)

    fun evaluate(std: Nutrition, co: Nutrition): Double {
        var ret = 0.0
        ret += abs(energy - std.energy) / std.energy * co.energy
        ret += abs(protein - std.protein) / std.protein * co.protein
        ret += abs(lipid - std.lipid) / std.lipid * co.lipid
        ret += abs(carbohydrate - std.carbohydrate) /
                std.carbohydrate * co.carbohydrate
        ret += abs(sodium - std.sodium) / std.sodium * co.sodium
        return ret
    }

    fun same(r: Nutrition): Boolean {
        val eps = Float.MIN_VALUE
        return abs(energy - r.energy) < eps &&
                abs(protein - r.protein) < eps &&
                abs(lipid - r.lipid) < eps &&
                abs(carbohydrate - r.carbohydrate) < eps &&
                abs(sodium - r.sodium) < eps
    }
}
