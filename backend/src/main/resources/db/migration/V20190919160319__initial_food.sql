-- セブンイレブン
INSERT INTO food (name, cost, energy, protein, lipid, carbohydrate, sodium)
VALUES('BIGポークフランク', 148, 336, 10, 29.5, 7.6, 712);
INSERT INTO food (name, cost, energy, protein, lipid, carbohydrate, sodium)
VALUES('燻製あらびきフランク', 100, 200, 7.2, 18.6, 1, 393);
INSERT INTO food (name, cost, energy, protein, lipid, carbohydrate, sodium)
VALUES('ビッグアメリカンドッグ', 100, 324, 6, 17.3, 37.7, 477);
INSERT INTO food (name, cost, energy, protein, lipid, carbohydrate, sodium)
VALUES('からあげ棒', 128, 194, 8, 12.5, 12.7, 418);
INSERT INTO food (name, cost, energy, protein, lipid, carbohydrate, sodium)
VALUES('ななからx5', 250, 400, 21, 25, 24, 735);
INSERT INTO food (name, cost, energy, protein, lipid, carbohydrate, sodium)
VALUES('揚げ鶏（竜田）', 175, 193, 12.6, 11.3, 10.5, 493);
INSERT INTO food (name, cost, energy, protein, lipid, carbohydrate, sodium)
VALUES('とり竜田', 180, 195, 13.6, 11.6, 9.3, 479);
INSERT INTO food (name, cost, energy, protein, lipid, carbohydrate, sodium)
VALUES('ななチキ', 188, 197, 13.8, 11.1, 10.7, 568);
INSERT INTO food (name, cost, energy, protein, lipid, carbohydrate, sodium)
VALUES('ささみ揚げ（梅しそ）', 128, 140, 10.2, 5.6, 12.3, 442);
INSERT INTO food (name, cost, energy, protein, lipid, carbohydrate, sodium)
VALUES('サクッとメンチ', 128, 262, 7.9, 19.1, 14.8, 282);
INSERT INTO food (name, cost, energy, protein, lipid, carbohydrate, sodium)
VALUES('ごろっごろ男爵の牛肉コロッケ', 88, 222, 3.6, 14.7, 19.7, 232);
INSERT INTO food (name, cost, energy, protein, lipid, carbohydrate, sodium)
VALUES('パリッと五目春巻(豚肉と筍)', 100, 160, 2.9, 11.5, 11.9, 256);
INSERT INTO food (name, cost, energy, protein, lipid, carbohydrate, sodium)
VALUES('フライドポテト', 150, 206, 3.3, 9.9, 27.5, 627);
INSERT INTO food (name, cost, energy, protein, lipid, carbohydrate, sodium)
VALUES('サクッとフライドポテト', 150, 231, 3.5, 11.1, 30.6, 666);
INSERT INTO food (name, cost, energy, protein, lipid, carbohydrate, sodium)
VALUES('倍盛りポテト', 278, 373, 5.9, 17.9, 49.8, 1135);
INSERT INTO food (name, cost, energy, protein, lipid, carbohydrate, sodium)
VALUES('サクッと倍盛りポテト', 278, 457, 7, 21.9, 60.8, 1320);
INSERT INTO food (name, cost, energy, protein, lipid, carbohydrate, sodium)
VALUES('ハッシュポテト', 82, 193, 1.5, 14.5, 15.1, 302);
INSERT INTO food (name, cost, energy, protein, lipid, carbohydrate, sodium)
VALUES('豚バラ串（しお味）', 130, 134, 5, 12.3, 0.7, 205);
INSERT INTO food (name, cost, energy, protein, lipid, carbohydrate, sodium)
VALUES('炭火焼き鳥串 ももタレ', 120, 85, 11, 3.8, 1.7, 414);
INSERT INTO food (name, cost, energy, protein, lipid, carbohydrate, sodium)
VALUES('炭火焼き鳥串 もも塩', 120, 69, 10.4, 3.0, 0.4, 325);
INSERT INTO food (name, cost, energy, protein, lipid, carbohydrate, sodium)
VALUES('炭火焼き鳥串 皮タレ', 129, 109, 5.7, 8.8, 1.7, 255);
INSERT INTO food (name, cost, energy, protein, lipid, carbohydrate, sodium)
VALUES('炭火焼き鳥 つくね串（3個刺し）', 119, 80, 7.9, 3.8, 3.7, 406);

INSERT INTO food (name, cost, energy, protein, lipid, carbohydrate, sodium)
VALUES('牛丼', 398, 645, 17, 29, 78.9, 907);

