import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';

const base_url = 'http://localhost:8090/api/v1';
const food_url = base_url + '/food';
const recommend_url = base_url + '/recommend';
const report_url = base_url + '/report';
const standard_url = base_url + '/standard';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/food',
      name: 'food',
      component: () => import('./views/Food.vue'),
      props: {base_url: food_url},
    },
    {
      path: '/stats',
      name: 'stats',
      component: () => import('./views/Stats.vue'),
      props: {
        report_url: report_url,
        standard_url: standard_url,
      },
    },
    {
      path: '/today',
      name: 'today',
      component: () => import('./views/Today.vue'),
      props: {
        recommend_url: recommend_url,
        report_url: report_url,
        standard_url: standard_url,
      },
    },
    {
      path: '/recommend',
      name: 'recommend',
      component: () => import('./views/Recommend.vue'),
      props: {
        recommend_url: recommend_url,
        report_url: report_url,
        standard_url: standard_url,
      },
    },
    {
      path: '/entry',
      name: 'entry',
      component: () => import('./views/Entry.vue'),
      props: {
        food_url: food_url,
        report_url: report_url,
      },
    },
  ],
});
